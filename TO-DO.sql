/* Procedimiento que elimina los datos de un cliente por privacidad */
CREATE OR REPLACE PROCEDURE BORRAR_USUARIO (idusuario mc_client.client_cod%type)
IS

BEGIN
    DELETE FROM MC_CLIENT CASCADE WHERE CLIENT_COD = idusuario;
    
    DBMS_OUTPUT.PUT_LINE('El usuario con número '||idusuario||' ha sido eliminado');
END borrar_usuario;
/

/* Comprobación */
INSERT INTO MC_CLIENT
VALUES (200,'RAPITA SPORTS','Vista Alegre, nº 6', 'Sant Carles de la Ràpita', 'ES', '43570', 415, '977897654', 7844, 5000, 'Es gallego');

CALL BORRAR_USUARIO(200);

-- NO FUNCIONA, NECESARIO AÑADIR EN LAS FOREIGN KEYS: ON DELETE CASCADE


