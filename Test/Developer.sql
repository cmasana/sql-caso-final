/* Comprobación de creación de precedimientos */
SET SERVEROUTPUT ON;

CREATE OR REPLACE PROCEDURE testeo
IS
    frase VARCHAR2 (42) := 'Hello World!';
BEGIN
    DBMS_OUTPUT.put_line (frase); 
END testeo;
/
-----
BEGIN
    testeo;
END;
/

SELECT * FROM CARMAS.mc_client;