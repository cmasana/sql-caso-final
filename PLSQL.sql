/* PROCEDIMIENTOS PARA IMPLEMENTAR EN NUESTRA BBDD */
-- SET SERVEROUTPUT ON;

/* Procedimiento que muestra todos los productos de una determinada comanda (factura) */
CREATE OR REPLACE PROCEDURE PED_POR_COM (id_com mc_detall.com_num%TYPE)
IS
    v_des mc_producte.descripcio%TYPE;
    
    CURSOR c_ped_com IS
        SELECT prod_num, preu_venda, quantitat, import
        FROM mc_detall
        WHERE com_num = id_com;

BEGIN
    FOR r_ped_com IN c_ped_com LOOP
        SELECT descripcio
        INTO v_des
        FROM mc_producte
        WHERE prod_num = r_ped_com.prod_num;
        
        DBMS_OUTPUT.PUT_LINE('Ref: '||r_ped_com.prod_num||' Producto: '||v_des||' Precio: '||r_ped_com.preu_venda||' Qty: '||r_ped_com.quantitat||' Importe: '||r_ped_com.import);
    END LOOP;
END;
/

/* Comprobación */
-- CALL PED_POR_COM(606);


/* Procedimiento que muestra los pedidos que ha hecho un cliente determinado */
CREATE OR REPLACE PROCEDURE PEDIDOS_CLI (id_client mc_comanda.client_cod%TYPE)
IS  
    v_nom mc_client.nom%TYPE;
    
    CURSOR c_pedidos_tot IS
        SELECT com_data, com_num, data_tramesa, total 
        FROM mc_comanda
        WHERE client_cod = id_client
        ORDER BY com_data;
        
BEGIN
    -- Consulta que almacena en una variable el nombre del cliente
    SELECT nom INTO v_nom
    FROM mc_client
    WHERE client_cod = id_client;
    
    -- Líneas con texto informativo
    DBMS_OUTPUT.PUT_LINE('Pedidos de: '||v_nom||' - Nº Cliente: '||id_client);
    DBMS_OUTPUT.PUT_LINE(' ');
    
    -- Bucle para obtener los registros del cursor
    FOR r_pedidos_tot IN c_pedidos_tot LOOP
        DBMS_OUTPUT.PUT_LINE('FACTURA: '||r_pedidos_tot.com_num||' --- '||r_pedidos_tot.com_data||' '||r_pedidos_tot.total);
        
        -- Llamada al procedimiento anterior para mostrar la lista de productos por comanda
        PED_POR_COM(r_pedidos_tot.com_num);
        
        DBMS_OUTPUT.PUT_LINE('');
    END LOOP;
END PEDIDOS_CLI;
/

/* Comprobación */
-- CALL PEDIDOS_CLI(100);

PAUSE "PROCEDIMIENTOS QUE EJECUTA EL CLIENTE: PEDIDOS_CLI Y PED_POR_COM CREADOS. PRESIONE UNA TECLA PARA CONTINUAR..."

/* Procedimiento que recibe por parámetro un cliente y devuelve los últimos 10 pedidos del cliente */
CREATE OR REPLACE PROCEDURE DIEZ_ULT_PED (id_client mc_comanda.client_cod%TYPE)
IS  
    v_nom mc_client.nom%TYPE;
    
    CURSOR c_pedidos_tot IS
        SELECT com_data, com_num, data_tramesa, total 
        FROM mc_comanda
        WHERE client_cod = id_client
        ORDER BY com_data
        FETCH FIRST 10 ROWS ONLY;
        
BEGIN
    -- Consulta que almacena en una variable el nombre del cliente
    SELECT nom INTO v_nom
    FROM mc_client
    WHERE client_cod = id_client;
    
    -- Líneas con texto informativo
    DBMS_OUTPUT.PUT_LINE('Pedidos de: '||v_nom||' - Nº Cliente: '||id_client);
    DBMS_OUTPUT.PUT_LINE(' ');
    
    -- Bucle para obtener los registros del cursor
    FOR r_pedidos_tot IN c_pedidos_tot LOOP
        DBMS_OUTPUT.PUT_LINE('FACTURA: '||r_pedidos_tot.com_num||' --- '||r_pedidos_tot.com_data||' '||r_pedidos_tot.total);
        
        DBMS_OUTPUT.PUT_LINE('');
    END LOOP;
END DIEZ_ULT_PED;
/

/* Comprobación */
-- CALL DIEZ_ULT_PED(100);


/* Procedimiento que muestra las ventas totales de un empleado en un determinado período de tiempo */
CREATE OR REPLACE PROCEDURE VENTAS_TOT_EMP (i_fecha VARCHAR2, f_fecha VARCHAR2, id_emp NUMBER)
IS  
    v_cognom mc_emp.cognom%TYPE;
    
    CURSOR c_ventas_tot IS
        SELECT a.com_data, a.com_num, a.client_cod, a.data_tramesa, a.total, t.repr_cod 
        FROM mc_comanda a, mc_client t
        WHERE a.com_data BETWEEN i_fecha AND f_fecha
        AND a.client_cod = t.client_cod
        AND t.repr_cod = id_emp
        ORDER BY a.com_data;
        
BEGIN
    -- Consulta que almacena el apellido del empleado en una variable
    SELECT cognom INTO v_cognom
    FROM mc_emp
    WHERE emp_no = id_emp;
    
    -- Líneas con texto informativo
    DBMS_OUTPUT.PUT_LINE('Empleado '||v_cognom|| ' nº: '||id_emp);
    DBMS_OUTPUT.PUT_LINE('Ventas entre: '||i_fecha|| ' y '||f_fecha);
    DBMS_OUTPUT.PUT_LINE(' ');
    
    -- Bucle para obtener los registros del cursor
    FOR r_ventas_tot IN c_ventas_tot LOOP
        DBMS_OUTPUT.PUT_LINE(r_ventas_tot.com_data||' --- '||r_ventas_tot.com_num||' '||r_ventas_tot.client_cod||' '||r_ventas_tot.total);
    END LOOP;
END VENTAS_TOT_EMP;
/

/* Testeo */
-- CALL VENTAS_TOT_EMP('01-05-86','12-01-87',7844);

/* Comprobación 
SELECT * FROM mc_comanda a
INNER JOIN mc_client t
ON t.client_cod = a.client_cod
INNER JOIN mc_emp e
ON t.repr_cod = e.emp_no
AND t.repr_cod = 7844
AND a.com_data BETWEEN '01-05-86' AND '12-01-87';
*/

PAUSE "PROCEDIMIENTOS QUE EJECUTA DPTO. VENTAS: DIEZ_ULT_PED Y VENTAS_TOT_EMP CREADOS. PRESIONE UNA TECLA PARA CONTINUAR..."

/* Procedimiento que muestra las ventas totales de todos los empleados en un determinado período de tiempo */
CREATE OR REPLACE PROCEDURE VENTAS_TOT (i_fecha VARCHAR2, f_fecha VARCHAR2)
IS  
    
    CURSOR c_ventas_tot IS
        SELECT com_data, com_num, client_cod, data_tramesa, total 
        FROM mc_comanda
        WHERE com_data BETWEEN i_fecha AND f_fecha
        ORDER BY com_data;
        
BEGIN
    -- Líneas con texto informativo
    DBMS_OUTPUT.PUT_LINE('Ventas entre: '||i_fecha|| ' y '||f_fecha);
    DBMS_OUTPUT.PUT_LINE(' ');
    
    -- Bucle para obtener los registros del cursor
    FOR r_ventas_tot IN c_ventas_tot LOOP
        DBMS_OUTPUT.PUT_LINE(r_ventas_tot.com_data||' --- '||r_ventas_tot.com_num||' '||r_ventas_tot.client_cod||' '||r_ventas_tot.total);
    END LOOP;
END VENTAS_TOT;
/

/* Comprobación */
-- CALL VENTAS_TOT('01-05-86','12-01-87');

PAUSE "PROCEDIMIENTO QUE EJECUTA EL JEFE DE VENTAS: VENTAS_TOT CREADO. PRESIONE UNA TECLA PARA CONTINUAR..."

/* Función que recibe por parámetro un campo e incrementa su valor un % (Calculo de IVA, aumentar comisiones, sueldos, etc) */
CREATE OR REPLACE FUNCTION INCRE_PER_CENT (i_valor NUMBER, i_increm NUMBER)
RETURN NUMBER
IS
  xcien CONSTANT NUMBER := 100; -- Para hallar el % 
  
BEGIN
    RETURN i_valor + (i_valor * i_increm / xcien);
END INCRE_PER_CENT;
/

/* Comprobación */
-- SELECT emp_no, cognom, ofici, salari AS "Antiguo Salario", INCRE_PER_CENT(salari, 20) AS "Nuevo Salario" FROM mc_emp;
-- SELECT emp_no, cognom, ofici, salari, comissio, INCRE_PER_CENT(comissio, 1) AS "Nueva comisión" FROM mc_emp WHERE emp_no = 7369;

PAUSE "FUNCIÓN QUE EJECUTAN VARIOS DPTOS: INCRE_PER_CENT CREADA. PRESIONE UNA TECLA PARA CONTINUAR..."

/* Función que calcula los años transcurridos entre dos fechas (fecha de baja - fecha de alta) */
CREATE OR REPLACE FUNCTION ANYS_TREBALLATS (i_date VARCHAR2, f_date VARCHAR2)
RETURN NUMBER
IS
    anys_tot NUMBER;
    anyo CONSTANT NUMBER := 12; -- Meses que tiene 1 año

BEGIN
    -- Actualización: Si añades la función ABS no importa el orden en el que introduzcas las fechas 
    SELECT ABS(ROUND((MONTHS_BETWEEN(f_date, i_date) / anyo), 0))
    INTO anys_tot
    FROM DUAL;
    
    RETURN anys_tot;
END ANYS_TREBALLATS;
/

/* Comprobación 
DECLARE
    anys_tot NUMBER;
BEGIN
    DBMS_OUTPUT.PUT_LINE(anys_treballats('01-01-30','01-01-20'));
END;

*/

PAUSE "FUNCIÓN QUE EJECUTA DPTO RRHH: ANYS_TREBALLATS CREADA. PRESIONE UNA TECLA PARA CONTINUAR..."

/* Creación de tabla backup para almacenar empleados antiguos */
CREATE TABLE MC_OLD_EMP (
    EMP_NO NUMBER(6,0),
    COGNOM VARCHAR(50),
    DEPT_NO NUMBER(3,0),
    DATA_ALTA DATE,
    DATA_BAIXA DATE,
    ANYS_TREB NUMBER(2,0)
);

/* Trigger que registra en una nueva tabla los datos de antiguos trabajadores cuando causan baja en la empresa */
CREATE OR REPLACE TRIGGER OLD_EMP 
BEFORE DELETE ON MC_EMP
FOR EACH ROW
BEGIN
   INSERT INTO MC_OLD_EMP
   VALUES (:old.emp_no, :old.cognom, :old.dept_no, :old.data_alta, sysdate, anys_treballats(sysdate, :old.data_alta));
END OLD_EMP;
/

/* Comprobación */
-- INSERT INTO MC_EMP VALUES (7999,'MASANA','VENEDOR', 7698, 28/05/19, 20000, null, 10);
-- DELETE FROM MC_EMP WHERE EMP_NO = 7999;

PAUSE "TRIGGER: OLD_EMP CREADO. PRESIONE UNA TECLA PARA CONTINUAR..."

/* Función que recibe por parámetro la id de un cliente y devuelve el nº de su última comanda */
CREATE OR REPLACE FUNCTION ULT_COM (cod_cli mc_comanda.client_cod%type)
RETURN mc_comanda%rowtype
IS
    ultima_comanda mc_comanda%rowtype;

BEGIN   
	SELECT * into ultima_comanda from mc_comanda 
	WHERE mc_comanda.client_cod = cod_cli
	order by com_data DESC
	FETCH FIRST 1 ROWS ONLY;
    
RETURN ultima_comanda;
END;
/

--declarem la variable ultima_comanda del tipus rowtype
--assignem a la variable el nom de la funció 
--imprimim la variable amb el nom del camp que volem retornar
DECLARE
    ultima_comanda mc_comanda%rowtype;
BEGIN
    ultima_comanda := ult_com(101);
    DBMS_OUTPUT.PUT_LINE(ultima_comanda.com_num);
END;
/

PAUSE "FUNCIÓN QUE EJECUTA EL CLIENTE: ULT_COM CREADA. PRESIONE UNA TECLA PARA CONTINUAR..."

/* Creamos la tabla que almacena los puntos por ventas */
CREATE TABLE mc_ptos_por_venta(
    emp_no NUMBER(4) not null,
    fecha date not null,
    quantitat_prod Number(9) not null,
    puntaje NUMBER(9) null,
    CONSTRAINT ptos_por_venta_pk
        PRIMARY KEY (emp_no)
);

/* Función que transforma el nº de ventas a puntos (Cada 10 productos vendidos, un empleado suma 5 puntos) */
CREATE OR REPLACE FUNCTION f_ventas_a_ptje (v_quantitat_prod in mc_detall.quantitat%TYPE)
--que devuelva dato en una variable de tipo int para que la división sea 
RETURN NUMBER 
IS 
    v_total INT;
    v_cant CONSTANT INT := 10; -- Productos
    v_point CONSTANT INT := 5; -- Puntos

BEGIN         
    v_total := (v_quantitat_prod / v_cant) * v_point;
    
    Return v_total;
    
END f_ventas_a_ptje;
/

/* Procedimiento que calcula el nº de puntos por empleado */
CREATE OR REPLACE PROCEDURE p_ptosporventa 
IS
    v_ptos int;
    cursor c_ptosxventa is
        SELECT mc_client.REPR_COD, sum(QUANTITAT) as suma
        FROM mc_detall, mc_client, mc_comanda, mc_emp
        where mc_DETALL.COM_NUM = mc_COMANDA.COM_NUM
        and mc_comanda.client_cod = mc_client.client_cod
        and mc_CLIENT.REPR_COD = mc_EMP.EMP_NO
        group by mc_client.REPR_COD;
  
BEGIN
    FOR r_ptosporventa IN c_ptosxventa loop
        v_ptos := f_ventas_a_ptje(r_ptosporventa.suma);
        
        /* imprimimos por pantalla*/       
        DBMS_OUTPUT.PUT_LINE('NRO.emp: '||r_ptosporventa.repr_cod||'  cant_prod: '||r_ptosporventa.suma||'  puntos: '||v_ptos);
    end loop;
end p_ptosporventa;
/

PAUSE "FUNCIÓN Y PROCEDIMIENTO QUE EJECUTA EL DPTO RRHH: F_VENTAS_A_PTJE Y P_PTOSPORVENTA CREADOS. PRESIONE UNA TECLA PARA CONTINUAR..."
